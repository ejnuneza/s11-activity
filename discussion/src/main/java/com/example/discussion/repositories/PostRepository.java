package com.example.discussion.repositories;
import com.example.discussion.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//An interface marked as @Repository contains methods for database manipulation
@Repository

//By extending CrudRepository, Post Repository will inherit its pre-defined methods for creating, retrieving, updating, and deleting records
// Post is the data type of the data used in the methods
// Object is the data type of the data returned from the database
public interface PostRepository extends CrudRepository<Post, Object> {
}


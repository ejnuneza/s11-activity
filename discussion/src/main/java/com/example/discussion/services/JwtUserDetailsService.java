package com.example.discussion.services;

import com.example.discussion.models.User;
import com.example.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

// This defines a service that is used by "Spring Security" during "authentication" to load user data from the UserRepository. It retrieves the user data by the given username, creates a new instance of "UserDetails" with the retrieved user data, and returns it. This implementation can be used in a "web application to authenticate users using JWT tokens".

//marks the JwtUserDetailsService class as a Spring component
@Component
public class JwtUserDetailsService implements UserDetailsService {

    // To inject userRepository instance into the class
    @Autowired
    private UserRepository userRepository;

    // Overrides the loadUserByUsername method of the UserDetailsService interface.
    @Override

    // "loadUserByUsername()" this is used to load user-specific data for authentication, this represents the username of the user to be loaded.
    // This method is usually called by Spring Security during authentication to load the user data.
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //retrieves the user data from the "UserRepository by the given "username"
        User user = userRepository.findByUsername(username);

        // Checks if the user data is null or not. If it is null, it will throw a UsernameNotFoundException with the given message.
        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }

        // This creates and returns a new instance of the UserDetails interface, which represents the authenticated user.
        // The implementation is provided by the Spring Security and is used to store the "username", "password" and list of granted authorities.
        // In this code, the user's username and password are taken from the User object retrieved from the UserRepository, and an empty list of authorities is passed to the constructor.
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), new ArrayList<>());
    }
}

